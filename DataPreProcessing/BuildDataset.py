import csv
import logging
import os

import pandas as pd

from DataPreProcessing.DataGetter import DataGetter


class DatasetBuilder:
    def __init__(self, postcode):
        self.postcode = postcode
        self.dataget = DataGetter(postcode)
        self.main_df_list = []
        self.data_directory = os.getcwd() + '/raw_dataset/'

    def build_dataset(self, year_min, year_max):
        save_name = self.data_directory + "dataset_" + self.postcode
        for y in range(year_min, year_max+1):
            request_response = self.dataget.get_file(y)
            decoded_response = request_response.content.decode('utf-8')
            csv_file = csv.reader(decoded_response.splitlines(), delimiter=',')

            current_dataset = pd.DataFrame(list(csv_file))
            self.main_df_list.append(current_dataset)

        master_df = pd.concat(self.main_df_list)
        master_df.columns = master_df.iloc[0]
        master_df.drop([0], inplace=True)
        logging.info("successfully concatenated dataframes")
        print(master_df)
        master_df.to_pickle(save_name)
        logging.info("concatenated df saved under: " + save_name)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    myDsBuilder = DatasetBuilder("03200")
    myDsBuilder.build_dataset(2014, 2019)

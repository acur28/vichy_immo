import numpy as np
import seaborn as sns

import json
from geopy import Nominatim

import os
import BuildDataset
from DataPreProcessing import DataPreCleaner

my_databuilder = BuildDataset.DatasetBuilder()
my_data_pt = DataPreCleaner.DatasetPreT()

if os.path.exists('Vichy_transaction_data'):
    pass
else:
    for year in [2014, 2015, 2016, 2017, 2018, 2019]:
        my_databuilder.download_df(year)
    my_databuilder.save_master_df()

main_df = my_data_pt.load_df()
df_main_processed = my_data_pt.pretreatment_main_df(main_df)
df_maison = my_data_pt.pretreatment_maison_df(df_main_processed)

import os

import numpy as np
import pandas as pd

features_of_interest = ['date_mutation', 'valeur_fonciere', 'adresse_numero',
                        'adresse_nom_voie', 'code_postal',
                        'nom_commune', 'code_departement', 'type_local',
                        'surface_reelle_bati', 'nombre_pieces_principales',
                        'surface_terrain', 'longitude', 'latitude']

class DatasetCleaner:
    def __init__(self, df, foi=None):
        if foi is None:
            self.foi = features_of_interest
        self.df = df
        self.data_directory = os.getcwd() + '/raw_dataset/'

    def preprocess(self):
        self.__clean_df()
        self.__cat_terrain()
        self.__save_preprocessed_df()

    def __clean_df(self):

        # filter the dataset retaining only the useful features

        filtered_df = self.df[self.foi]
        # keep only some transactions types
        filtered_df = filtered_df.loc[
            (filtered_df['type_local'] == 'Maison') | (filtered_df['type_local'] == 'Appartement')]

        filtered_df = filtered_df.dropna(
            subset=['code_postal', 'valeur_fonciere',
                    'nom_commune', 'nombre_pieces_principales',
                    'adresse_numero', 'longitude', 'latitude'])

        # convert compatible strings to int, which will ease the processing later on
        filtered_df['nombre_pieces_principales'] = filtered_df['nombre_pieces_principales'].astype(int)
        filtered_df['adresse_numero'] = filtered_df['adresse_numero'].astype(int)
        filtered_df['valeur_fonciere'] = filtered_df['valeur_fonciere'].astype(int)
        filtered_df['code_postal'] = filtered_df['code_postal'].astype(str)

        return filtered_df

    def __cat_terrain(self):
        terrain_series = self.df['surface_terrain']
        print(terrain_series)
        bins = [-np.inf, 0, 50, 125, 175, 300, 500, np.inf]
        tags = ['0 - 50 m2', '50 - 125 m2', '125 - 175 m2', '175 - 300 m2', '300 - 500 m2', '+500m2']
        binned_terrain = pd.cut(terrain_series, bins, False, tags)

        self.df['cat_terrain'] = binned_terrain

    def __save_preprocessed_df(self):
        save_name = self.data_directory + "dataset_preprocessed"
        self.df.to_pickle(save_name)
        # logging.info("preprocessed saved under: " + save_name)

    def __cat_princing(self, prix):
        if 1000 < prix < 1500:
            ret = '1000 - 1500 €/M2'
        elif 1500 <= prix < 2000:
            ret = '1500 - 2000 €/M2'
        elif 2000 <= prix < 3000:
            ret = '2000 - 3000 €/M2'
        elif 3000 <= prix < 4000:
            ret = '3000 - 4000 €/M2'
        elif prix > 4000:
            ret = '+ 4000 €/M2'
        else:
            ret = np.nan

        return ret

    def __complete_cp(self, cp):
        if len(cp) < 5:
            new_cp = str(0) + cp
        else:
            new_cp = cp
        return new_cp

    def __extract_year(self, string):
        try:
            ret = string[0:4]
        except:
            ret = string
        return ret

    def load_df(self):
        return pd.read_pickle('Vichy_transaction_data')

    def pretreatment_main_df(self, df):
        # turn relevant values to int
        df['code_postal'] = df['code_postal'].map(self.__complete_cp)
        df['Terrain'] = df['surface_terrain'].map(self.__cat_terrain)
        df['date_mutation'] = df['date_mutation'].map(self.__extract_year)

        # removing useless columns
        df = df.drop(['surface_terrain'], axis=1)
        df = df.drop(['code_departement'], axis=1)
        df = df.drop(['nom_commune'], axis=1)
        df = df.drop(['code_postal'], axis=1)
        # df = df.drop(['date_mutation'], axis=1)
        df = df.drop(['adresse_numero'], axis=1)
        df = df.drop(['adresse_nom_voie'], axis=1)

        df = df.reset_index()
        if 'index' in df.columns:
            df = df.drop(['index'], axis=1)

        # create new price/sqmeters feature
        df['prix_m2'] = df['valeur_fonciere'] / df['surface_reelle_bati']
        df = df.drop(['valeur_fonciere'], axis=1)
        df['prix_m2'] = df['prix_m2'].map(round)

        return df_filtered

    def pretreatment_maison_df(self, df):
        # df_appart= df.loc(df['type_local'] == 'Appartement')
        df_maison = df.loc(df['type_local'] == 'Maison')
        # df_appart = df_appart.drop(['type_local'], axis=1)
        df_maison = df_maison.drop(['type_local'], axis=1)
        # df_appart = df_appart.drop(['Terrain'], axis=1)

        # df_appart = df_appart.reset_index().drop(['index'], axis=1)
        df_maison.reset_index().drop(['index'], axis=1)

        df_maison_filtered = df_maison[
            (df_maison['surface_reelle_bati'] < 300) &
            (df_maison['surface_reelle_bati'] > 25) &
            (df_maison['nombre_pieces_principales'] < 7) &
            (df_maison['surface_terrain'] < 500)
            ]
        return df_maison_filtered


if __name__ == "__main__":
    df = pd.read_pickle("raw_dataset/dataset_03200")
    datacleaningops = DatasetCleaner(df)
    datacleaningops.preprocess()

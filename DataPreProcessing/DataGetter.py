import logging
import os
import shutil

import requests


class DataGetter:

    def __init__(self, postcode):
        self.postcode = str(postcode)
        self.department = str(postcode)[0:2]

    def get_file(self, year):
        base_url = 'https://cadastre.data.gouv.fr/data/etalab-dvf/latest/csv/'
        str_year = str(year)
        url = base_url + str_year + "/communes/" + self.department + "/" + self.postcode + ".csv"
        file = requests.get(url)
        if file.status_code == 200:
            logging.info('csv file reached at: ' + url)
        else:
            logging.warning('CSV file not reachable at: ' + url)
            raise Exception("CSV file not reachable at: " + url)
        return file

    def clear_image_directory(self):
        """
        this method should always be called first
        :return: nothing
        """
        folder = self.data_directory
        try:
            print('Cleaning existing pictures')
            for filename in os.listdir(folder):
                file_path = os.path.join(folder, filename)
                try:
                    if os.path.isfile(file_path) or os.path.islink(file_path):
                        os.unlink(file_path)
                    elif os.path.isdir(file_path):
                        shutil.rmtree(file_path)
                except Exception as e:
                    print('Failed to delete %s. Reason: %s' % (file_path, e))
        except FileNotFoundError:
            os.mkdir(folder)
            logging.warning('Image save directory did not exist. It has been created')


if __name__ == '__main__':
    dataget = DataGetter("05013")
    file = dataget.get_file(2019)
    print(file.text)

    file = dataget.get_file(2013)
    print(file.text)

    dataget = DataGetter("050013")
    file = dataget.get_file(2019)
    print(file.text)
